# iris

iris - Images Resized In Seconds

A simple, configurable image resizer built in Python intended for use with mobile app icons.